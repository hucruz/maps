<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Http\Request;

// use Storage;

class PathController extends BaseController
{
    public function __construct(){
        $this->response = array("claves" => array(), "ruta" => array(), "nodos" => array(), "steps" => array());
        $this->all_nodos = json_decode(file_get_contents(base_path() . "/storage/app/all_nodos.json"), TRUE);
        $this->all_routes = json_decode(file_get_contents(base_path() . "/storage/app/w_routes.json"), TRUE);
        $this->keys_all_nodos = array_keys($this->all_nodos);
        $this->added_nodos = [];
        $this->rutas = [];
    }
    public function dijkstra($graphs, $source, $target){
        $vertices = array();
        $neighbours = array();
        $path = array();

        foreach ($graphs as $key => $value) {
            foreach ($value as $k => $v) {
                array_push($vertices, $key, $k);
                $neighbours[$key][] = array('endEdge' => $k, 'cost' => $v);
            }
        }

        $vertices = array_unique($vertices);

        foreach ($vertices as $vertex) {
            $dist[$vertex] = INF;
            $previous[$vertex] = NULL;
        }

        $dist[$source] = 0;
        $g = $vertices;

        while (count($g) > 0){
            $min = INF;
            foreach ($g as $vertex) {
                if ($dist[$vertex] < $min){
                    $min = $dist[$vertex];
                    $u = $vertex;
                }
            }

            $g = array_diff($g, array($u));
            if ($dist[$u] == INF or $u == $target){
                break;
            }

            if (isset($neighbours[$u])) {
                foreach ($neighbours[$u] as $arr) {
                    $alt = $dist[$u] + $arr["cost"];
                    if ($alt < $dist[$arr["endEdge"]]) {
                        $dist[$arr["endEdge"]] = $alt;
                        $previous[$arr["endEdge"]] = $u;
                    }
                }
            }
        }

        $u = $target;
        while (isset($previous[$u])) {
            array_unshift($path, $u);
            $u = $previous[$u];
        }

        array_unshift($path, $u);
        return $path;
    }
    function approx($point, $nodos) {
        $v = explode(",", $point);
        $lon = (float) $v[0];
        $lat = (float) $v[1];
        $min = INF;
        $auxlat = null;
        $auxlon = null;
        foreach ($nodos as $value) {
            $pp = explode(",", $value);
            $plon = (float) $pp[0];
            $plat = (float) $pp[1];
            $dlon = abs($plon - $lon);
            $dlat = abs($plat - $lat);
            $dd = sqrt($dlon * $dlon + $dlat * $dlat);
            if ($dd < $min){
                $min = $dd;
                $auxlon = $plon;
                $auxlat = $plat;
            }
        }
        $rr = $auxlon . "," . $auxlat;
        return $rr;
    }
    public function addArco($arco, $name,$num){

        if (!isset($this->response["ruta"][$num])){
            $this->response["ruta"][$num] = array("name" => $name, "values" => array());
        }
        foreach ($arco as $value) {
            // $neededObject = array_filter(
            //     $this->response["ruta"][$name],
            //     function ($e) use ($value){
            //         return $e["location"]["lng"] == $value[0] && $e["location"]["lat"] == $value[1];
            //     }
            // );
            // if (!$neededObject){
                array_push($this->response["ruta"][$num]["values"], array("location"=>array("lng" => $value[0] , "lat" => $value[1])));
            // }
        }
    }
    public function addNodo($nodo){
        array_push($this->response["nodos"], $nodo);
    }
    public function addStep($step){
        array_push($this->response["steps"], $step);
    }
    public function formatPath($path){
        $arco = array();
        foreach ($path as $key => $value) {
            $v = explode(",", $value);
            array_push($arco, array((float) $v[0], (float) $v[1]));
        }
        if (count($path) == 1){
            $v = explode(",", $path[0]);
            array_push($arco, array((float) $v[0], (float) $v[1]));
        }
        return $arco;
    }
    public function findNodos($path){
        $tmp = array();
        foreach ($path as $value) {
            if (isset($this->all_nodos[$value])){
                $v = explode(",", $value);
                $nodo = array(
                    "nombre" => $this->all_nodos[$value],
                    "location" => array("lng" => (float)$v[0], "lat" => (float)$v[1])
                    );
                array_push($tmp, $value);
                if (!in_array($value, $this->added_nodos)){
                    $this->addNodo($nodo);
                    array_push($this->added_nodos, $value);

                }
            }
        }
        return $tmp;
    }
    public function findPumaRoutes($a){

    }
    function index(Request $request){

        if (!$request->has("start")){
            return "no start value";
        }
        if (!$request->has("end")){
            return "no end value";
        }

        $nodos = json_decode(file_get_contents(base_path() . "/storage/app/nodos.json"), TRUE);

        $graphs = json_decode(file_get_contents(base_path() . "/storage/app/w_routes.json"), TRUE);

        $StartAt = explode(",",$request->input("start"));
        $EndAt = explode(",",$request->input("end"));

        $StartAt = implode(",", array($StartAt[1],$StartAt[0]));
        $EndAt = implode(",",array($EndAt[1],$EndAt[0]));

        $start = $this->approx($StartAt,$this->keys_all_nodos);
        $end = $this->approx($EndAt,$this->keys_all_nodos);

        $first = $this->approx($start,$nodos);
        $last = $this->approx($end,$nodos);

        $camina1 = $this->dijkstra($this->all_routes, $start,$first);
        $camina2 = $this->dijkstra($this->all_routes, $last,$end);
        $path = $this->dijkstra($graphs, $first, $last);

        $arco = $this->formatPath($camina1);
        $this->addArco($arco, "unknow",0);
        $arco = $this->formatPath($path);
        $this->addArco($arco, "unknow",1);
        $arco = $this->formatPath($camina2);
        $this->addArco($arco, "unknow",2);

        $this->findNodos($camina1);
        $this->findNodos($path);
        $this->findNodos($camina2);

        return json_encode($this->response);
    }
    function bus(Request $request){

        if (!$request->has("start")){
            return "no start value";
        }
        if (!$request->has("end")){
            return "no end value";
        }
        set_time_limit(300);
        $nodos = json_decode(file_get_contents(base_path() . "/storage/app/pumabus_nodos.json"), TRUE);

        $graphs = json_decode(file_get_contents(base_path() . "/storage/app/w_pumabus.json"), TRUE);
        $pumaRoutes = json_decode(file_get_contents(base_path() . "/storage/app/rutas_pumabus.json"), TRUE);

        // Next four lines swap coords from "long,lat" to "lat,long"
        $StartAt = explode(",",$request->input("start"));
        $EndAt = explode(",",$request->input("end"));
        $StartAt = implode(",", array($StartAt[1],$StartAt[0]));
        $EndAt = implode(",",array($EndAt[1],$EndAt[0]));

        $start = $this->approx($StartAt,$this->keys_all_nodos);
        $end = $this->approx($EndAt,$this->keys_all_nodos);

        $first = $this->approx($start,array_keys($nodos));
        $last = $this->approx($end,array_keys($nodos));

        $camina1 = $this->dijkstra($this->all_routes, $start,$first);
        $camina2 = $this->dijkstra($this->all_routes, $last,$end);
        $path = $this->dijkstra($graphs, $first, $last);

        $this->findNodos($camina1);
        $pumaNodos = $this->findNodos($path);
        $this->findNodos($camina2);

        $arco = $this->formatPath($camina1);
        $this->addArco($arco, "camina 1", 0);

        $rindice = 0;
        foreach ($path as $key => $value) {
            $rname = $pumaRoutes[$value][0];
            if (count($this->rutas) != 0 && in_array(end($this->rutas),$pumaRoutes[$value])){
                $rname = end($this->rutas);
            } else{
                if ($rname != end($this->rutas)){
                  array_push($this->rutas,$rname);
                  $rindice++;
                }
                /*
                if (!in_array($rname, $this->rutas)){
                    array_push($this->rutas, $rname);
                }*/

            }
            $v = explode(",", $value);
            $arco = array(array((float)$v[0],(float)$v[1]));
            $this->addArco($arco, $rname, $rindice);
        }
        // return "";
        $arco = $this->formatPath($camina2);
        $this->addArco($arco, "camina 2", $rindice +1);


        $this->response["claves"] = array_keys($this->response["ruta"]);
        return json_encode($this->response);

    }

    function bici(Request $request){

        if (!$request->has("start")){
            return "no start value";
        }
        if (!$request->has("end")){
            return "no end value";
        }

        $nodos = json_decode(file_get_contents(base_path() . "/storage/app/bicipuma_nodos.json"), TRUE);

        $graphs = json_decode(file_get_contents(base_path() . "/storage/app/w_bici.json"), TRUE);

        $StartAt = explode(",",$request->input("start"));
        $EndAt = explode(",",$request->input("end"));

        $StartAt = implode(",", array($StartAt[1],$StartAt[0]));
        $EndAt = implode(",",array($EndAt[1],$EndAt[0]));

        $start = $this->approx($StartAt,$this->keys_all_nodos);
        $end = $this->approx($EndAt,$this->keys_all_nodos);

        $first = $this->approx($start,array_keys($nodos));
        $last = $this->approx($end,array_keys($nodos));

        $camina1 = $this->dijkstra($this->all_routes, $start,$first);
        $camina2 = $this->dijkstra($this->all_routes, $last,$end);
        $path = $this->dijkstra($graphs, $first, $last);

        $arco = $this->formatPath($camina1);
        $this->addArco($arco, "camina 1",0);
        $arco = $this->formatPath($path);
        $this->addArco($arco, "bicipuma",1);
        $arco = $this->formatPath($camina2);
        $this->addArco($arco, "camina 2",2);

        $this->findNodos($camina1);
        $this->findNodos($path);
        $this->findNodos($camina2);

        $this->response["steps"] = array();
        if ($start != $first){
            array_push($this->response["steps"], array("location" => array("lon" => (float) explode(",",$start)[0],"lat" => (float) explode(",",$start)[1]),"msg" => "Camina desde " . $this->all_nodos[$start] . " hasta " . $nodos[$first]));
        } else {
            array_push($this->response["steps"], array("location" => array("lon" => (float) explode(",",$start)[0],"lat" => (float) explode(",",$start)[1]),"msg" => "Camina hasta " . $nodos[$first]));
        }
        array_push($this->response["steps"], array("location" => array("lon" => (float) explode(",",$first)[0],"lat" => (float) explode(",",$first)[1]),"msg" => "Toma tu bici aquí en " . $nodos[$first] . " y avanza hasta " . $nodos[$last]));
        array_push($this->response["steps"], array("location" => array("lon" => (float) explode(",",$last)[0],"lat" => (float) explode(",",$last)[1]),"msg" => "Tu viaje en bicipuma termina aquí en " . $nodos[$last]));
        if ($last != $end){
            array_push($this->response["steps"], array("location" => array("lon" => (float) explode(",",$last)[0],"lat" => (float) explode(",",$last)[1]),"msg" => "Camina a " . $this->all_nodos[$end]));
        }
        $this->response["claves"] = array_keys($this->response["ruta"]);
        return json_encode($this->response);
    }
    function peaton(Request $request){

        if (!$request->has("start")){
            return "no start value";
        }
        if (!$request->has("end")){
            return "no end value";
        }

        $nodos = json_decode(file_get_contents(base_path() . "/storage/app/peaton_nodos.json"), TRUE);

        $graphs = json_decode(file_get_contents(base_path() . "/storage/app/w_peaton.json"), TRUE);

        $StartAt = explode(",",$request->input("start"));
        $EndAt = explode(",",$request->input("end"));

        $StartAt = implode(",", array($StartAt[1],$StartAt[0]));
        $EndAt = implode(",",array($EndAt[1],$EndAt[0]));

        $start = $this->approx($StartAt,$this->keys_all_nodos);
        $end = $this->approx($EndAt,$this->keys_all_nodos);

        $first = $this->approx($start,$nodos);
        $last = $this->approx($end,$nodos);

        $camina1 = $this->dijkstra($this->all_routes, $start,$first);
        $camina2 = $this->dijkstra($this->all_routes, $last,$end);
        $path = $this->dijkstra($graphs, $first, $last);

        $arco = $this->formatPath($camina1);
        $this->addArco($arco, "camina 1",0);
        $arco = $this->formatPath($path);
        $this->addArco($arco, "camina 1",1);
        $arco = $this->formatPath($camina2);
        $this->addArco($arco, "camina 1",2);

        $this->findNodos($camina1);
        $this->findNodos($path);
        $this->findNodos($camina2);
        $this->response["claves"] = array_keys($this->response["ruta"]);
        return json_encode($this->response);
    }
}
