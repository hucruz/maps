<?php

$app->get('/', 'PathController@index');

$app->get('/pumabus', 'PathController@bus');
$app->get('/bicipuma', 'PathController@bici');
$app->get('/peaton', 'PathController@peaton');

$app->get('/steps', 'PathController@steps');
